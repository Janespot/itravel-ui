"use client"
import React from 'react'
import { Axios } from 'axios'
import { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { faXmark } from '@fortawesome/free-solid-svg-icons'
import Nav from "@/app/components/CRM/Nav"
import { Heading } from "@kiwicom/orbit-components"
import './styles.css';

export function Itinerary() {
    const [fullName, setFullName]                       = useState("");
    const [emailAddress, setEmailAddress]               = useState("");
    const [phoneNumber, setPhoneNumber]                 = useState("");
    const [address, setAddress]                         = useState("");
    const [flightNumber, setFlightNumber]               = useState("");
    const [flightType, setFlightType]                   = useState("");
    const [departingFrom, setDepartingFrom]             = useState("");
    const [destination, setDestination]                 = useState("");
    const [departureDate, setDepartureDate]             = useState("");
    const [departureTime, setDepartureTime]             = useState("");
    const [arrivalDate, setArrivalDate]                 = useState("");
    const [arrivalTime, setArrivalTime]                 = useState("");
    const [adults, setAdults]                           = useState("");
    const [children, setChildren]                       = useState("");
    const [infants, setInfants]                         = useState("");
    const [transferType, setTransferType]               = useState("");
    const [flightPrice, setFlightPrice]                 = useState("");
    const [accommodationPrice, setAccommodationPrice]   = useState("");
    const [transferPrice, setTransferPrice]             = useState("");
    const [totalPrice, setTotalPrice]                   = useState("");
    const [notes, setNotes]                             = useState("");
    const [flightValues, setFlightValues]               = useState([{
                                                                        flightNumber: "",
                                                                        flightType: "",
                                                                        departingFrom: "",
                                                                        destination: "",
                                                                        departureDate: ""    ,
                                                                        departureTime: "",
                                                                        arrivalDate: "",
                                                                        arrivalTime: "",
                                                                        adults: "",
                                                                        children:"",
                                                                        infants: ""
                                                                    }]);
    const [accommodationValues, setAccommodationValues] = useState([{
                                                                        hotelName: "",
                                                                        rooms: "",
                                                                        checkinDate: "",
                                                                        checkoutDate: "",
                                                                        roomType: "",
                                                                        mealType: "",
                                                                        accAdults: "",
                                                                        accChildren: "",
                                                                        accInfants: ""
                                                                    }]);

    const addItinerary = ()=>{
        Axios.post("http://localhost:3002/api/create", {
            fullName: fullName, 
            emailAddress: emailAddress, 
            phoneNumber: phoneNumber, 
            address: address, 
            transferType: transferType, 
            flightPrice: flightPrice, 
            accommodationPrice: accommodationPrice, 
            transferPrice: transferPrice, 
            totalPrice: totalPrice,
            departingFrom: departingFrom, 
            destination: destination, 
            departureDate: departureDate, 
            departureTime: departureTime, 
            arrivalDate: arrivalDate, 
            arrivalTime: arrivalTime, 
            adults1: adults1, 
            children1: children1, 
            infants1: infants1, 
            hotelName: hotelName, 
            checkinDate: checkinDate, 
            checkoutDate: checkoutDate, 
            mealType: mealType, 
            roomType: roomType, 
            rooms: rooms, 
            adults2: adults2, 
            children2: children2, 
            infants2: infants2 
        })
    }

    var hourOptions   = "<option>00</option>";
    var minOptions    = "<option>00</option>";

    // useEffect(() => {
    //     const departureHours = Array.from(
    //         document.getElementsByClassName("departureHourOptions")
    //     );

    //     const arrivalHours = Array.from(
    //         document.getElementsByClassName("arrivalHourOptions")
    //     );

    //     const departureMins = Array.from(
    //         document.getElementsByClassName("departureMinutesOptions")
    //     );

    //     const arrivalMins = Array.from(
    //         document.getElementsByClassName("arrivalMinutesOptions")
    //     );

    //     for(var i = 1; i < 24; i++) {
    //         hourOptions += i < 10 ? "<option>0" + i + "</option>" : "<option>" + i + "</option>";
    //     }
        
    //     departureHours.forEach(element => {
    //         element.innerHTML += hourOptions;
    //     });

    //     arrivalHours.forEach(element => {
    //         element.innerHTML += hourOptions;
    //     })

    //     for(var j = 1; j < 60; j++) {
    //         minOptions += j < 10 ? "<option>0" + j + "</option>": "<option>" + j + "</option>";
    //     }

    //     departureMins.forEach(element => {
    //         element.innerHTML += minOptions;
    //     });

    //     arrivalMins.forEach(element => {
    //         element.innerHTML += minOptions;
    //     })
    // }, []);

    function showTwoWay() {
        document.getElementById("showTwoWay").style.display="block";
        document.getElementById("showTwoWayRadio").checked = true;
        document.getElementById("showOneWayRadio").checked = false;
    }

    function showOneWay() {
        document.getElementById("showTwoWay").style.display="none";
        document.getElementById("showOneWayRadio").checked = true;
        document.getElementById("showTwoWayRadio").checked = false;
    }

    function showPrice() {
        var togglePrice = Array.from(
            document.getElementsByClassName("togglePrice")
        );

        togglePrice.forEach(element => {
            element.style.display="block";
        });
         
        document.getElementById("showPriceRadio").checked = true;
        document.getElementById("hidePriceRadio").checked = false;
    }

    function hidePrice() {
        var togglePrice = Array.from(
            document.getElementsByClassName("togglePrice")
        );

         togglePrice.forEach(element => {
            element.style.display="none";
         });

        document.getElementById("hidePriceRadio").checked = true;
        document.getElementById("showPriceRadio").checked = false;
    }

    let handleChange = (i, e) => {
        let newFlight               = [...flightValues];
        newFlight[i][e.target.name] = e.target.value;
        setFlightValues(newFlight);
    }

    let handleAccChange = (i, e) => {
        let newAccommodation                = [...accommodationValues];
        newAccommodation[i][e.target.name]  = e.target.value;
        setAccommodationValues(newAccommodation);
    }
    
    let addFlightValues = () => {
        setFlightValues([...flightValues, {}])
    }

    let addAccommodationValues = () => {
        setAccommodationValues([...accommodationValues, {}])
    }

    let removeFlight = (i) => {
        let newFlight = [...flightValues];
        newFlight.splice(i, 1);
        setFlightValues(newFlight);
    }

    let removeAccommodation = (i) => {
        let newAccommodation = [...accommodationValues];
        newAccommodation.splice(i, 1);
        setAccommodationValues(newAccommodation)
    }
    
    let handleSubmit = (e) => {
        e.preventDefault();

        let myarray             = JSON.parse(JSON.stringify(flightValues))
        let myaccommodation     = JSON.parse(JSON.stringify(accommodationValues))

        let transferType        = document.getElementById("transferType").value;

        if(document.getElementById("showPriceRadio").checked == true) {
            var flightPrice         = document.getElementById("flightPrice").value;
            var accommodationPrice  = document.getElementById("accommodationPrice").value;
            var transferPrice       = document.getElementById("transferPrice").value;
            var totalPrice          = document.getElementById("totalPrice").value;
        } else if(document.getElementById("hidePriceRadio").checked == true) {
            var flightPrice         = 0;
            var accommodationPrice  = 0;
            var transferPrice       = 0;
            var totalPrice          = 0;
        }

        let myItinerary = "Full Name: " + fullName + "\n"
            + "Email Address: " + emailAddress + "\n"
            + "Phone Number: " + phoneNumber + "\n"
            + "Address: " + address + "\n"
            + "Transfer Type: " + transferType + "\n"
            + "Flight Price: " + flightPrice + "\n"
            + "Accommodation Price: " + accommodationPrice + "\n"
            + "Transfer Price:" + transferPrice + "\n"
            +"Total Price: " + totalPrice + "\n"
            + "Notes: " + notes + "\n"

        alert(myItinerary);

        if(document.getElementById("showTwoWayRadio").checked == true) {
            let departureTime   = document.getElementById("departureTimeTwo").value || "00:00"
            let arrivalTime     = document.getElementById("arrivalTimeTwo").value || "00:00"

            let myFlight = "Flight Number: " + flightNumber + "\n"
                + "Flight Type: " + flightType + "\n"
                + "Departing From: " + departingFrom + "\n"
                + "Destination: " + destination + "\n"
                + "Departure Date: " + departureDate.toString() + "\n"
                + "Departure Time: " + departureTime + "\n"
                + "Arrival Date: " + arrivalDate + "\n"
                + "Arrival Time: " + arrivalTime + "\n"
                + "Number of Adults: " + adults + "\n"
                + "Number of Children: " + children + "\n"
                + "Number of Infants: " + infants + "\n"

                alert(myFlight)
                // console.log(departureDate)
        }

        for(var i = 0; i < myarray.length; i++) {
            let myFlightNumber      = myarray[i].flightNumber || ""
            let myFlightType        = myarray[i].flightType || ""
            let myDepartingFrom     = myarray[i].departingFrom || ""
            let myDestination       = myarray[i].destination || ""
            let myDepartureDate     = myarray[i].departureDate || ""
            let myDepartureTime     = myarray[i].departureTime || ""
            let myArrivalDate       = myarray[i].arrivalDate || ""
            let myArrivalTime       = myarray[i].arrivalTime || ""
            let myAdults            = myarray[i].adults || ""
            let myChildren          = myarray[i].children || ""
            let myInfants           = myarray[i].infants || ""

            let result = "Flight Number: " + myFlightNumber + "\n"
            +"Flight Type: " + myFlightType  + "\n"
            + "Departing From: " + myDepartingFrom  + "\n"
            + "Destination: " + myDestination  + "\n"
            + "Departure Date: " + myDepartureDate  + "\n"
            + "Departure Time: " + myDepartureTime  + "\n"
            + "Arrival Date: " + myArrivalDate  + "\n"
            + "Arrival Time: " + myArrivalTime  + "\n"
            + "Number of Adults: " + myAdults  + "\n"
            + "Number of Children: " + myChildren  + "\n"
            + "Number of Infants: " + myInfants  + "\n"
            
            alert(result)
            // console.log("jane")
            console.log(myDepartureTime)
        }

        for(var j = 0; j < myaccommodation.length; j++) {
            let myHotelName     = myaccommodation[j].hotelName || ""
            let myRooms         = myaccommodation[j].rooms || ""
            let myCheckinDate   = myaccommodation[j].checkinDate || ""
            let myCheckoutDate  = myaccommodation[j].checkoutDate || ""
            let myRoomType      = myaccommodation[j].roomType || ""
            let myMealType      = myaccommodation[j].mealType || ""
            let myAdults        = myaccommodation[j].accAdults || ""
            let myChildren      = myaccommodation[j].accChildren || ""
            let myInfants       = myaccommodation[j].accInfants || ""

            let result = "Hotel Name: " + myHotelName + "\n"
                + "Rooms: " + myRooms + "\n"
                + "Checkin Date: " + myCheckinDate + "\n"
                + "Checkout Date: " + myCheckoutDate + "\n"
                + "Room Type: " + myRoomType + "\n"
                + "Meal Type: " + myMealType + "\n"
                + "Number of Adults: " + myAdults + "\n"
                + "Number of Children: " + myChildren + "\n"
                + "Number of Infants: " + myInfants + "\n"
            
            alert(result)
        }
    }

    function getTotalPrice() {
        let flightPrice         = parseInt(document.getElementById("flightPrice").value) || 0;
        let accommodationPrice  = parseInt(document.getElementById("accommodationPrice").value) || 0;
        let transferPrice       = parseInt(document.getElementById("transferPrice").value) || 0;

        let totalPrice          = flightPrice + accommodationPrice + transferPrice;

        document.getElementById("totalPrice").value = totalPrice;
    }

    function departureTimeTwo() {
        let departureHour       = document.getElementById("departureHourTwo").value || "00";
        let departureMinutes    = document.getElementById("departureMinuteTwo").value || "00";
        
        let departureTimeTwo    = departureHour + ":" + departureMinutes;

        document.getElementById("departureTimeTwo").value = departureTimeTwo;
    }

    function arrivalTimeTwo() {
        let arrivalHour     = document.getElementById("arrivalHourTwo").value || "00";
        let arrivalMinute   = document.getElementById("arrivalMinuteTwo").value || "00";

        let arrivalTimeTwo  = arrivalHour + ":" + arrivalMinute;

        document.getElementById("arrivalTimeTwo").value = arrivalTimeTwo;
    }

    function myDepartureTime() {      
        let departureHour       = Array.from(
            document.getElementsByClassName("departureHourOptions")
        );
        

        let departureMinutes    = Array.from(
            document.getElementsByClassName("departureMinutesOptions")
        );
        
         let departureTime = Array.from(
            document.getElementsByClassName("myDepartureTime")
         )

        for(var i = 0; i < departureHour.length; i++) {
            let myHour = departureHour[i].value;
            let myMinute = departureMinutes[i].value;
            let myTime = departureTime[i].value = myHour + ":" + myMinute;
            alert(myTime)
        }

    }

    function myArrivalTime() {
        let arrivalHour = Array.from(
            document.getElementsByClassName("arrivalHourOptions")
        );

        let arrivalMinutes = Array.from(
            document.getElementsByClassName("arrivalMinutesOptions")
        );

        let arrivalTime = Array.from(
            document.getElementsByClassName("myArrivalTime")
        );

        for(var i = 0; i < arrivalHour.length; i++) {
            let myHour = arrivalHour[i].value;
            let myMinute = arrivalMinutes[i].value;
            let myTime = arrivalTime[i].value = myHour + ":" + myMinute;
        }
    }

    function dates(e) {
        e.currentTarget.type = "date"
        let mydate      = new Date();
        let newdate     = mydate.getDate();
        let month       = mydate.getMonth() + 1;
        let year        = mydate.getFullYear();
        if(newdate < 10) {
            newdate     = "0" + newdate;
        }
    
        if (month < 10) {
            month       = "0" + month;
        }
    
        newdate         = newdate.toString().padStart(2, "0");
        month           = month.toString().padStart(2, "0");

        let dates = `${year}-${month}-${newdate}`
        console.log(dates)
        e.currentTarget.setAttribute("min", dates)
    }

    return (
        <>
        <div className="flex flex-row py-20 w-full" style={{"position": "relative", "paddingTop": "60px", "paddingBottom": "70px", "marginTop": "0"}}>
            <Nav />
            <div style={{"marginLeft": "200px"}} className="w-full">
                <div className="container mx-auto py-20" style={{"paddingTop": "30px"}}>
                    <div className="my-10">
                        <Heading as="h1" type="title2">Itinerary</Heading>
                    </div>
            <form className='w-full' onSubmit={handleSubmit}>
                <div className='flex flex-col gap-4 w-full mb-5'>
                    <div className='text-center text-2xl mb-5 mt-5' style={{"color": "#2489b5"}}>
                        <h2><b>Client Details</b></h2>
                    </div>

                    <div className='flex flex-row w-full gap-8'>
                        <div className='basis-1/2'>
                            <label className='font-bold text-slate-500'>Full Name</label><br />
                            <input type="text" name="fullName" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setFullName(e.target.value)}}
                                placeholder = "Enter Full Name" />
                        </div>

                        <div className='basis-1/2'>
                            <label className='font-bold text-slate-500'>Email Address</label><br />
                            <input type="text" name="emailAddress" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setEmailAddress(e.target.value)}}
                                placeholder='Enter Email Address' />
                        </div>
                    </div>

                    <div className='flex flex-row w-full gap-8'>
                        <div className='basis-1/2'>
                            <label className='font-bold text-slate-500'>Phone Number</label><br />
                            <input type="text" name="phoneNumber" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setPhoneNumber(e.target.value)}}
                                placeholder='Enter Phone Number' />
                        </div>

                        <div className='basis-1/2'>
                            <label className='font-bold text-slate-500'>Address</label><br />
                            <input type="text" name="address" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setAddress(e.target.value)}}
                                placeholder='Enter Address' />
                        </div>
                    </div>
                </div>

                <div className="flex flex-col gap-4 mb-5 w-full">
                    <div className='text-center text-2xl mb-5 mt-5' style={{"color": "#2489b5"}}>
                        <h2><b>Flight Details</b></h2>
                    </div>

                    <div className='flex flex-row gap-8 mb-5 pl-3'>
                        <div className='basis-1/4' onClick={showTwoWay}>
                            <input type="radio" id="showTwoWayRadio" className='mr-3 py-1 shadow radioWay' />
                            <label className='font-bold text-slate-500'>Two way</label>
                        </div>

                        <div className='basis-1/4' onClick={showOneWay}>
                            <input type="radio" id="showOneWayRadio" className='mr-3 py-1 shadow' checked />
                            <label className='font-bold text-slate-500'>One Way</label>
                        </div>
                    </div>

                    <div style={{"backgroundColor": "#d4ecf7", "display": "none"}} className='py-3' id="showTwoWay">
                        <div className='flex flex-row w-full gap-8 mb-3'>
                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Flight Number</label><br />
                                <input type="text"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                placeholder='Enter flight Number' name="flightNumber"
                                onChange={e =>setFlightNumber(e.target.value)} autoComplete="off" />
                            </div>

                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Flight Type</label><br />
                                <select  className='pl-3 border py-1 rounded-sm w-full shadow'
                                placeholder='Enter flight Number' name="flightType"
                                onChange={(e) => setFlightType(e.target.value)}>
                                    <option value="">Select Type</option>
                                    <option value="Inbound">Inbound</option>
                                    <option value="Outbound">Outbound</option>
                                </select>
                            </div>
                        </div>

                        <div className='flex flex-row w-full gap-8 mb-3'>
                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Departing From</label><br />
                                <input type="text" name="departingFrom"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                    onChange={(e)=>{setDepartingFrom(e.target.value)}}
                                    placeholder='Flying from?' autoComplete="off" />
                            </div>

                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Destination</label><br />
                                <input type="text" name="destination"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                    onChange={(e)=>{setDestination(e.target.value)}}
                                    placeholder='Flying to?' autoComplete="off" />
                            </div>
                        </div>

                        <div className='flex flex-row w-full gap-8 mb-3'>
                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Departure Date</label><br />
                                <input type="text" name="departureDate" placeholder='dd/mm/yyyy' 
                                    className='pl-3 border py-1 rounded-sm w-full shadow dates'
                                    onClick={dates}
                                    onBlur={e => {!e.currentTarget.value ? e.currentTarget.type = "text" : e.currentTarget.type = "date"}}
                                    onChange={(e) => setDepartureDate(e.target.value)} 
                                    autoComplete="off" />
                            </div>

                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Departure Time</label><br />
                                <input id="departureTimeTwo" type="hidden" name="departureTime"
                                    onChange={(e)=>{setDepartureTime(e.target.value)}} defaultValue="00:00" />
                                    <div className='flex flex-row gap-4'>
                                        <select id="departureHourTwo" 
                                            className='pl-3 border py-1 rounded-sm w-full shadow hourOptions'
                                            onChange={departureTimeTwo}>
                                            <option>00</option>
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                            <option>21</option>
                                            <option>22</option>
                                            <option>23</option>
                                        </select>
                                        <select id="departureMinuteTwo" 
                                            className='pl-3 border py-1 rounded-sm w-full shadow minutesOptions'
                                            onChange={departureTimeTwo}>
                                            <option>00</option>
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                            <option>21</option>
                                            <option>22</option>
                                            <option>23</option>
                                            <option>24</option>
                                            <option>25</option>
                                            <option>26</option>
                                            <option>27</option>
                                            <option>28</option>
                                            <option>29</option>
                                            <option>30</option>
                                            <option>31</option>
                                            <option>32</option>
                                            <option>33</option>
                                            <option>34</option>
                                            <option>35</option>
                                            <option>36</option>
                                            <option>37</option>
                                            <option>38</option>
                                            <option>39</option>
                                            <option>40</option>
                                            <option>41</option>
                                            <option>42</option>
                                            <option>43</option>
                                            <option>44</option>
                                            <option>45</option>
                                            <option>46</option>
                                            <option>47</option>
                                            <option>48</option>
                                            <option>49</option>
                                            <option>50</option>
                                            <option>51</option>
                                            <option>52</option>
                                            <option>53</option>
                                            <option>54</option>
                                            <option>55</option>
                                            <option>56</option>
                                            <option>57</option>
                                            <option>58</option>
                                            <option>59</option>
                                        </select>
                                    </div>
                            </div>
                        </div>

                        <div className='flex flex-row w-full gap-8 mb-3'>
                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Arrival Date</label><br />
                                <input type="text" name="arrivalDate" placeholder="dd/mm/yyyy" 
                                    className='pl-3 border py-1 rounded-sm w-full shadow'
                                    onClick={dates}
                                    onBlur={e => {!e.currentTarget.value ? e.currentTarget.type="text" : e.currentTarget.type = "date"}}
                                    onChange={(e)=>{setArrivalDate(e.target.value)}} autoComplete="off" />
                            </div>

                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Arrival Time</label><br />
                                <input id="arrivalTimeTwo" type="hidden" name="arrivalTime"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                    onChange={(e)=>{setArrivalTime(e.target.value)}} defaultValue="00:00" />
                                    <div className='flex flex-row gap-4'>
                                        <select id="arrivalHourTwo" 
                                            className='pl-3 border py-1 rounded-sm w-full shadow hourOptions'
                                            onChange={arrivalTimeTwo}>
                                            <option>00</option>
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                            <option>21</option>
                                            <option>22</option>
                                            <option>23</option>
                                        </select>
                                        <select id="arrivalMinuteTwo" 
                                            className='pl-3 border py-1 rounded-sm w-full shadow minutesOptions'
                                            onChange={arrivalTimeTwo}>
                                            <option>00</option>
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                            <option>21</option>
                                            <option>22</option>
                                            <option>23</option>
                                            <option>24</option>
                                            <option>25</option>
                                            <option>26</option>
                                            <option>27</option>
                                            <option>28</option>
                                            <option>29</option>
                                            <option>30</option>
                                            <option>31</option>
                                            <option>32</option>
                                            <option>33</option>
                                            <option>34</option>
                                            <option>35</option>
                                            <option>36</option>
                                            <option>37</option>
                                            <option>38</option>
                                            <option>39</option>
                                            <option>40</option>
                                            <option>41</option>
                                            <option>42</option>
                                            <option>43</option>
                                            <option>44</option>
                                            <option>45</option>
                                            <option>46</option>
                                            <option>47</option>
                                            <option>48</option>
                                            <option>49</option>
                                            <option>50</option>
                                            <option>51</option>
                                            <option>52</option>
                                            <option>53</option>
                                            <option>54</option>
                                            <option>55</option>
                                            <option>56</option>
                                            <option>57</option>
                                            <option>58</option>
                                            <option>59</option>
                                        </select>
                                    </div>
                            </div>
                        </div>

                        <div className='flex flex-row w-full gap-8 mb-3'>
                            <div className='basis-1/3'>
                                <label className='font-bold text-slate-500'>Adults</label><br />
                                <input type="number" name="adults"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                    onChange={(e)=>{setAdults(e.target.value)}}
                                    placeholder='How many adults?' autoComplete='off' />
                            </div>

                            <div className='basis-1/3'>
                                <label className='font-bold text-slate-500'>Children</label><br />
                                <input type="number" name="children"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                    onChange={(e)=>{setChildren(e.target.value)}}
                                    placeholder='How many children?' autoComplete='off' />
                            </div>

                            <div className='basis-1/3'>
                                <label className='font-bold text-slate-500'>Infants</label><br />
                                <input type="number" name="infants"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                    onChange={(e)=>{setInfants(e.target.value)}}
                                    placeholder='How many infants?' autoComplete='off' />
                            </div>
                        </div>
                    </div>

                    {flightValues.map((element, index) => (
                        <div key={index} className="myAppendedFlight py-3">
                        <div className='flex flex-row w-full gap-8 mb-3'>
                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Flight Number</label><br />
                                <input type="text"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                placeholder='Enter flight Number' name="flightNumber"
                                value={element.flightNumber || ""} onChange={(e) => handleChange(index, e)}
                                autoComplete='off' />
                            </div>

                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Flight Type</label><br />
                                <select  className='pl-3 border py-1 rounded-sm w-full shadow'
                                 name="flightType" value={element.flightType || ""} 
                                 onChange={e => handleChange(index, e)}>
                                    <option value="">Select Type</option>
                                    <option value="Inbound">Inbound</option>
                                    <option value="Outbound">Outbound</option>
                                </select>
                            </div>
                        </div>

                        <div className='flex flex-row w-full gap-8 mb-3'>
                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Departing From</label><br />
                                <input type="text" name="departingFrom"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                    value={element.departingFrom || ""} onChange={(e)=> handleChange(index, e)}
                                    placeholder='Flying from?' autoComplete='off' />
                            </div>

                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Destination</label><br />
                                <input type="text" name="destination"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                    value={element.destination || ""} onChange={(e)=> handleChange(index, e)}
                                    placeholder='Flying to?' autoComplete='off' />
                            </div>
                        </div>

                        <div className='flex flex-row w-full gap-8 mb-3'>
                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Departure Date</label><br />
                                <input type="text" onClick={dates}
                                    onBlur={e => {!e.currentTarget.value ? e.currentTarget.type = "text" : e.currentTarget.type = "date"}} 
                                    name="departureDate" placeholder='dd/mm/yyyy' 
                                    className='pl-3 border py-1 rounded-sm w-full shadow'
                                    value={element.departureDate || ""} 
                                    onChange={(e)=> handleChange(index, e)}
                                    autoComplete='off' />
                            </div>

                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Departure Time</label><br />
                                <input type="hidden" name="departureTime" className='myDepartureTime'
                                    value={element.departureTime || "00:00"} onChange={(e)=> handleChange(index, e)} />
                                    <div className='flex flex-row gap-4'>
                                        <select className='pl-3 border py-1 rounded-sm w-full shadow departureHourOptions'
                                            onChange={myDepartureTime}>
                                            <option>00</option>
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                            <option>21</option>
                                            <option>22</option>
                                            <option>23</option>
                                        </select>
                                        <select className='pl-3 border py-1 rounded-sm w-full shadow departureMinutesOptions'
                                            onChange={myDepartureTime}>
                                            <option>00</option>
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                            <option>21</option>
                                            <option>22</option>
                                            <option>23</option>
                                            <option>24</option>
                                            <option>25</option>
                                            <option>26</option>
                                            <option>27</option>
                                            <option>28</option>
                                            <option>29</option>
                                            <option>30</option>
                                            <option>31</option>
                                            <option>32</option>
                                            <option>33</option>
                                            <option>34</option>
                                            <option>35</option>
                                            <option>36</option>
                                            <option>37</option>
                                            <option>38</option>
                                            <option>39</option>
                                            <option>40</option>
                                            <option>41</option>
                                            <option>42</option>
                                            <option>43</option>
                                            <option>44</option>
                                            <option>45</option>
                                            <option>46</option>
                                            <option>47</option>
                                            <option>48</option>
                                            <option>49</option>
                                            <option>50</option>
                                            <option>51</option>
                                            <option>52</option>
                                            <option>53</option>
                                            <option>54</option>
                                            <option>55</option>
                                            <option>56</option>
                                            <option>57</option>
                                            <option>58</option>
                                            <option>59</option>
                                        </select>
                                    </div>
                            </div>
                        </div>

                        <div className='flex flex-row w-full gap-8 mb-3'>
                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Arrival Date</label><br />
                                <input type="text" name="arrivalDate" placeholder='dd/mm/yyyy'
                                    className='pl-3 border py-1 rounded-sm w-full shadow'
                                    onClick={dates}
                                    onBlur={e => {!e.currentTarget.value ? e.currentTarget.type = "text" : e.currentTarget.type = "date"}}
                                    value={element.arrivalDate || ""} 
                                    onChange={(e)=> handleChange(index, e)}
                                    autoComplete='off' />
                            </div>

                            <div className='basis-1/2'>
                                <label className='font-bold text-slate-500'>Arrival Time</label><br />
                                <input type="hidden" name="arrivalTime"  className='myArrivalTime'
                                    value={element.arrivalTime || "00:00"} onChange={(e)=> handleChange(index, e)} />
                                    <div className='flex flex-row gap-4'>
                                        <select className='pl-3 border py-1 rounded-sm w-full shadow arrivalHourOptions'
                                            onChange={myArrivalTime}>
                                            <option>00</option>
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                            <option>21</option>
                                            <option>22</option>
                                            <option>23</option>
                                        </select>
                                        <select className='pl-3 border py-1 rounded-sm w-full shadow arrivalMinutesOptions'
                                            onChange={myArrivalTime}>
                                            <option>00</option>
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                            <option>21</option>
                                            <option>22</option>
                                            <option>23</option>
                                            <option>24</option>
                                            <option>25</option>
                                            <option>26</option>
                                            <option>27</option>
                                            <option>28</option>
                                            <option>29</option>
                                            <option>30</option>
                                            <option>31</option>
                                            <option>32</option>
                                            <option>33</option>
                                            <option>34</option>
                                            <option>35</option>
                                            <option>36</option>
                                            <option>37</option>
                                            <option>38</option>
                                            <option>39</option>
                                            <option>40</option>
                                            <option>41</option>
                                            <option>42</option>
                                            <option>43</option>
                                            <option>44</option>
                                            <option>45</option>
                                            <option>46</option>
                                            <option>47</option>
                                            <option>48</option>
                                            <option>49</option>
                                            <option>50</option>
                                            <option>51</option>
                                            <option>52</option>
                                            <option>53</option>
                                            <option>54</option>
                                            <option>55</option>
                                            <option>56</option>
                                            <option>57</option>
                                            <option>58</option>
                                            <option>59</option>
                                        </select>
                                    </div>
                            </div>
                        </div>

                        <div className='flex flex-row w-full gap-8 mb-3'>
                            <div className='basis-1/3'>
                                <label className='font-bold text-slate-500'>Adults</label><br />
                                <input type="number" name="adults"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                    value={element.adults || ""} onChange={(e)=> handleChange(index, e)}
                                    placeholder='How many adults?' autoComplete='off' />
                            </div>

                            <div className='basis-1/3'>
                                <label className='font-bold text-slate-500'>Children</label><br />
                                <input type="number" name="children"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                    value={element.children || ""} onChange={(e)=> handleChange(index, e)}
                                    placeholder='How many children?' autoComplete='off' />
                            </div>

                            <div className='basis-1/3'>
                                <label className='font-bold text-slate-500'>Infants</label><br />
                                <input type="number" name="infants"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                     value={element.infants || ""} onChange={(e)=>handleChange(index, e)}
                                    placeholder='How many infants?' autoComplete='off' />
                            </div>
                        </div>
                        {
                            index ?
                                <div className='flex flex-row-reverse'>
                                    <button type="button" className="p-3 rounded-md mt-5" 
                                        style={{"color": "white", "backgroundColor": "#ff4d4d"}}
                                        onClick={() => removeFlight(index)}>
                                        <i>Remove Flight</i></button>
                                </div>
                            : null
                        }
                    </div>
                        ))}

                    <div className="flex flex-row w-full my-5">
                        <button type="button" className="p-3 rounded-sm" onClick={()=>addFlightValues()} style={{"color": "#d4ecf7", "backgroundColor": "#2489b5"}}>
                            <FontAwesomeIcon icon={faPlus} /> Add Flight</button>
                    </div>
                </div>

                <div className='flex flex-col mb-5 w-full gap-4 mb-5'>
                    <div className='text-center text-2xl mb-5 mt-5' style={{"color": "#2489b5"}}>
                        <h2><b>Accommodation Details</b></h2>
                    </div>

                    {accommodationValues.map((element, index) => (
                        <div key={index} className='addedAccommodation py-3'>
                            <div className='flex flex-row w-full gap-8 mb-3'>
                                <div className='basis-1/2'>
                                    <label className='font-bold text-slate-500'>Hotel Name</label><br />
                                    <input type="text" name="hotelName"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                        value={element.hotelName || ""} onChange={(e)=>{handleAccChange(index, e)}}
                                        placeholder='Enter hotel name' autoComplete='off' />
                                </div>

                                <div className='basis-1/2'>
                                    <label className='font-bold text-slate-500'>Rooms</label><br />
                                    <input type="number" name="rooms"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                        value={element.rooms ||""} onChange={(e)=>{handleAccChange(index, e)}}
                                        placeholder='How many rooms?' autoComplete='off' />
                                </div>
                            </div>

                            <div className='flex flex-row w-full gap-8 mb-3'>
                                <div className='basis-1/2'>
                                    <label className='font-bold text-slate-500'>Checkin Date</label><br />
                                    <input type="text" name="checkinDate" placeholder="dd/mm/yyyy"
                                        className='pl-3 border py-1 rounded-sm w-full shadow'
                                        value={element.checkinDate ||""} 
                                        onClick={dates}
                                        onBlur={e => {!e.currentTarget.value ? e.currentTarget.type = "text" : e.currentTarget.type = "date"}}
                                        onChange={(e)=>{handleAccChange(index, e)}}
                                        autoComplete='off' />
                                </div>

                                <div className='basis-1/2'>
                                    <label className='font-bold text-slate-500'>Checkout Date</label><br />
                                    <input type="text" name="checkoutDate" placeholder="dd/mm/yyyy"
                                        className='pl-3 border py-1 rounded-sm w-full shadow'
                                        onClick={dates}
                                        onBlur={e => {!e.currentTarget.value ? e.currentTarget.type="text" : e.currentTarget.type = "date"}}
                                        value={element.checkoutDate || ""} onChange={(e)=>{handleAccChange(index, e)}}
                                        autoComplete='off' />
                                </div>
                            </div>

                            <div className='flex flex-row w-full gap-8 mb-3'>
                                <div className='basis-1/2'>
                                    <label className='font-bold text-slate-500'>Room Type</label><br />
                                    <input type="text" name="roomType"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                        value={element.roomType || ""} onChange={(e)=>{handleAccChange(index, e)}}
                                        placeholder='Enter room type' />
                                </div>

                                <div className='basis-1/2'>
                                    <label className='font-bold text-slate-500'>Meal Type</label><br />
                                    <input type="text" name="mealType"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                        value={element.mealType || ""} onChange={(e)=>{handleAccChange(index, e)}}
                                        placeholder='Enter meal type' />
                                </div>
                            </div>

                            <div className='flex flex-row w-full gap-8 mb-3'>
                                <div className='basis-1/3'>
                                    <label className='font-bold text-slate-500'>Adults</label><br />
                                    <input type="number" name="accAdults"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                        value={element.accAdults || ""} onChange={(e)=>{handleAccChange(index, e)}}
                                        placeholder='How many adults?' autoComplete='off' />
                                </div>

                                <div className='basis-1/3'>
                                    <label className='font-bold text-slate-500'>Children</label><br />
                                    <input type="number" name="accChildren"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                        value={element.accChildren || ""} onChange={(e)=>{handleAccChange(index, e)}}
                                        placeholder='How many children?' autoComplete='off' />
                                </div>

                                <div className='basis-1/3'>
                                    <label className='font-bold text-slate-500'>Infants</label><br />
                                    <input type="number" name="accInfants"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                        value={element.accInfants || ""} onChange={(e)=>{handleAccChange(index, e)}}
                                        placeholder='How many infants?' autoComplete='off' />
                                </div>
                            </div>
                            {
                                index ?
                                <div className='flex flex-row-reverse'>
                                    <button type="button" className="p-3 rounded-md mt-5" 
                                        style={{"color": "white", "backgroundColor": "#ff4d4d"}}
                                        onClick={() => removeAccommodation(index)}>
                                        <i>Remove Accommodation</i></button>
                                </div>
                                : null
                            }
                        </div>
                    ))}

                    <div className="flex flex-row w-full my-5">
                        <button type="button" className="p-3 rounded-sm" onClick={() => addAccommodationValues()} style={{"color": "#d4ecf7", "backgroundColor": "#2489b5"}}>
                            <FontAwesomeIcon icon={faPlus} /> Add Accommodation</button>
                    </div>
                </div>

                <div className='flex flex-col mb-5 w-full gap-4'>
                    <div className='text-center text-2xl mb-5 mt-5' style={{"color": "#2489b5"}}>
                        <h2><b>Transfer Details</b></h2>
                    </div>

                    <div className='flex flex-row gap-8 w-full'>
                        <div className='basis-1/2'>
                            <label className='font-bold text-slate-500'>Transfer Type</label><br />
                            <select name="transferType"  id="transferType" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setTransferType(e.target.value)}}>
                                    <option value="">Select Transfer</option>
                                    <option value="Private Transfer">Private Transfer</option>
                                    <option value="Shared Transfer">Shared Transfer</option>
                                    <option value="Speedy Transfer">Speedy Transfer</option>
                                </select>
                        </div>
                        <div className='basis-1/2 invisible'>
                            <label className='font-bold text-slate-500'>Transfer Type</label><br />
                            <input type="text" name="hiddenTransfer"  className='pl-3 border py-1 rounded-sm w-full shadow' />
                        </div>
                    </div>
                </div>

                <div className='flex flex-col gap-4 mb-5 w-full'>
                    <div className='text-center text-2xl mb-5 mt-5' style={{"color": "#2489b5"}}>
                        <h2><b>Price Details</b></h2>
                    </div>

                    <div className='flex flex-row gap-8 mb-5 pl-3'>
                        <div className='basis-1/4' onClick={showPrice}>
                            <input type="radio" id="showPriceRadio" className='mr-3 py-1 shadow' checked />
                            <label className='font-bold text-slate-500'>Show Price</label>
                        </div>

                        <div className='basis-1/4' onClick={hidePrice}>
                            <input type="radio" id="hidePriceRadio" className='mr-3 py-1 shadow' />
                            <label className='font-bold text-slate-500'>Hide Price</label>
                        </div>
                    </div>

                    <div className='flex flex-row gap-8 w-full'>
                        <div className='basis-1/4 togglePrice'>
                            <label className='font-bold text-slate-500'>Flight Price</label><br />
                            <input id="flightPrice" type="text" name="flightPrice"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                onInput={getTotalPrice} placeholder='£ 0.00' autoComplete='off' />
                        </div>

                        <div className='basis-1/4 togglePrice'>
                            <label className='font-bold text-slate-500'>Accommodation Price</label><br />
                            <input id="accommodationPrice" type="text" name="accommodationPrice"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                onInput={getTotalPrice} placeholder='£ 0.00' autoComplete='off' />
                        </div>

                        <div className='basis-1/4 togglePrice'>
                            <label className='font-bold text-slate-500'>Transfer Price</label><br />
                            <input id="transferPrice" type="text" name="transferPrice"  className='pl-3 border py-1 rounded-sm w-full shadow'
                                onInput={getTotalPrice} placeholder='£ 0.00' autoComplete='off' />
                        </div>

                        <div className='basis-1/4 togglePrice'>
                            <label className='font-bold text-slate-500'>Total Price</label><br />
                            <input id="totalPrice" type="readonly" name="totalPrice"  className='pl-3 border py-1 rounded-sm w-full bg-slate-200'
                                onChange={(e)=>{setTotalPrice(e.target.value)}} readonly />
                        </div>
                    </div>

                    <div>
                        <label className='font-bold text-slate-500'>Itinerary Notes</label><br />
                        <textarea name="notes" rows="8" className='border py-1 rounded-sm w-full shadow pl-3'
                            onChange={(e)=>{setNotes(e.target.value)}} placeholder="Itinerary notes here..."
                            autoComplete='off'></textarea>
                    </div>
                </div>

                <div>
                    <input type="submit" name="submit" value="Save Itinerary" 
                        style={{["backgroundColor"]: "#2489b5"}} className="py-2 font-bold rounded-md w-full text-center text-white"
                            // onClick={addItinerary} 
                            />
                </div>
            </form>
                </div>
            </div>
        </div>
        </>
    )
}