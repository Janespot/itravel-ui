"use client"
import { useState, useEffect } from "react"
import axios from "axios"
import Nav from "../Nav"
import { Heading } from "@kiwicom/orbit-components"
const cors = require("cors");

export function Enquiries() {
    const [fullName, setFullName]           = useState("");
    const [emailAddress, setEmailAddress]   = useState("");
    const [phoneNumber, setPhoneNumber]     = useState("");
    const [address, setAddress]             = useState("");
    const [adults, setAdults]               = useState("");
    const [children, setChildren]           = useState("");
    const [infants, setInfants]             = useState("");
    const [rooms, setRooms]                 = useState("");
    const [assignTo, setAssignTo]           = useState("");
    const [status, setStatus]               = useState("");
    const [priority, setPriority]           = useState("");
    const [category, setCategory]           = useState("");
    const [notes, setNotes]                 = useState("");

    const addEnquiry = ()=>{
        axios.post("http://jsonplaceholder.typicode.com/posts", {
            data: {
                fullName: fullName,
                emailAddress: emailAddress, 
                phoneNumber: phoneNumber, 
                address: address, 
                adults: adults, 
                children: children, 
                infants: infants, 
                rooms: rooms, 
                assignTo: assignTo, 
                status: status, 
                priority: priority, 
                category: category, 
                notes: notes
            }
            }).then((response)=>{
                console.log(response)
            }).catch(err => {console.log(err)})
    }

    return (
        <>
        <div className="flex flex-row mb-0" style={{"position": "relative"}}>
            <Nav />
            <div style={{"marginLeft": "200px"}} className="w-full">
                <div className="container mx-auto py-20">
                    <div className="mb-10">
                        <Heading as="h1" type="title2">Enquiry Form</Heading>
                    </div>
            <form className="flex flex-col gap-2 w-full">
               <div className="flex flex-col gap-4 w-full mb-5 mt-5">
                    <div className="text-center text-2xl mb-5 mt-5" style={{["color"]: "#2489b5"}}>
                        <h2><b>Client Details</b></h2>
                    </div>

                    <div className="flex flex-row gap-8 w-full">
                        <div className="basis-1/2">
                            <label className='font-bold text-slate-500'>Full Name</label><br />
                            <input type="text" name="fullName" className='border py-1 rounded-sm w-full shadow pl-3'
                                onChange={(e)=>{setFullName(e.target.value)}} 
                                placeholder = "e.g. John Doe"/>
                        </div>

                        <div className="basis-1/2">
                            <label className='font-bold text-slate-500'>Email Address</label><br />
                            <input type="text" name="emailAddress" className='border py-1 rounded-sm w-full shadow pl-3'
                                onChange={(e)=>{setEmailAddress(e.target.value)}}
                                placeholder="e.g. example@gmail.com" />
                        </div>
                    
                    </div>

                    <div className=" flex flex-row gap-8 w-full">
                        <div className="basis-1/2">
                                <label className='font-bold text-slate-500'>Phone Number</label><br />
                                <input type="text" name="phoneNumber" className='border py-1 rounded-sm w-full shadow pl-3'
                                    onChange={(e)=>{setPhoneNumber(e.target.value)}}
                                    placeholder="e.g. 999-999-9999" />
                        </div>

                        <div className="basis-1/2">
                                <label className='font-bold text-slate-500'>Address</label><br />
                                <input type="text" name="address" className='border py-1 rounded-sm w-full shadow pl-3'
                                    onChange={(e)=>{setAddress(e.target.value)}}
                                    placeholder="Enter address" />
                        </div>
                    </div>
                </div>

                <div className="flex flex-col gap-4 mb-5">
                    <div className="text-center text-2xl mb-5 mt-5" style={{["color"]: "#2489b5"}}>
                        <h2><b>Occupants</b></h2>
                    </div>

                    <div className="flex flex-row gap-8">
                        <div className="basis-1/2">
                                <label className='font-bold text-slate-500'>Adults</label><br />
                                <input type="number" name="adults" className='border py-1 rounded-sm w-full shadow pl-3'
                                    onChange={(e)=>{setAdults(e.target.value)}}
                                    placeholder="how many adults?" />
                        </div>

                        <div className="basis-1/2">
                                <label className='font-bold text-slate-500'>Children</label><br />
                                <input type="number" name="children" className='border py-1 rounded-sm w-full shadow pl-3'
                                    onChange={(e)=>{setChildren(e.target.value)}}
                                    placeholder="how many children?" />
                        </div>
                    </div>

                    <div className="flex flex-row gap-8">
                        <div className="basis-1/2">
                                <label className='font-bold text-slate-500'>Infants</label><br />
                                <input type="number" name="infants" className='border py-1 rounded-sm w-full shadow pl-3'
                                    onChange={(e)=>{setInfants(e.target.value)}}
                                    placeholder="how many infants?" />
                        </div>
                        
                        <div className="basis-1/2">
                                <label className='font-bold text-slate-500'>Rooms</label><br />
                                <input type="number" name="rooms" className='border py-1 rounded-sm w-full shadow pl-3'
                                    onChange={(e)=>{setRooms(e.target.value)}}
                                    placeholder="how many rooms?" />
                        </div>
                    </div>
                </div>

                <div className="flex flex-col gap-4 mb-5">
                    <div className="text-center text-2xl mb-5 mt-5" style={{["color"]: "#2489b5"}}>
                        <h2><b>Enquiry Details</b></h2>
                    </div>

                    <div className="flex flex-row gap-8">
                        <div className="basis-1/2">
                                <label className='font-bold text-slate-500'>Assign To</label><br />
                                <input type="text" name="assignTo" className='border py-1 rounded-sm w-full shadow pl-3'
                                    onChange={(e)=>{setAssignTo(e.target.value)}} />
                        </div>

                        <div className="basis-1/2">
                                <label className='font-bold text-slate-500'>Status</label><br />
                                <select type="text" name="status" className='border py-1 rounded-sm w-full shadow pl-3'
                                    onChange={(e)=>{setStatus(e.target.value)}}>
                                    <option disabled>Select Status</option>
                                    <option>Awaiting Agent</option>
                                    <option>Working On It</option>
                                    <option>Waiting On Customer</option>
                                    <option>Booked</option>
                                    <option>Closed</option>
                                </select>
                        </div>
                    </div>

                    
                    <div className="flex flex-row gap-8">
                        <div className="basis-1/2">
                                <label className='font-bold text-slate-500'>Priority</label><br />
                                <select type="text" name="priority" className='border py-1 rounded-sm w-full shadow pl-3'
                                    onChange={(e)=>{setPriority(e.target.value)}}>
                                    <option disabled value="">Select Priority</option>
                                    <option value="High">High</option>
                                    <option value="Medium">Medium</option>
                                    <option value="Low">Low</option>
                                </select>
                        </div>

                        <div className="basis-1/2">
                                <label className='font-bold text-slate-500'>Category</label><br />
                                <input type="text" name="category" className='border py-1 rounded-sm w-full shadow pl-3'
                                    onChange={(e)=>{setCategory(e.target.value)}} />
                        </div>
                    </div>

                    <div>
                        <label className='font-bold text-slate-500'>Enquiry Notes</label><br />
                        <textarea name="notes" rows="8" className='border py-1 rounded-sm w-full shadow pl-3'
                            onChange={(e)=>{setNotes(e.target.value)}} placeholder="Enquiry notes here..."></textarea>
                    </div>
                </div>

                <div>
                    <input type="button" name="submit" value="Save Enquiry" 
                        style={{["backgroundColor"]: "#2489b5"}} className="py-2 font-bold rounded-md w-full text-center text-white"
                        onClick={addEnquiry} />
                </div>
            </form>
                </div>
            </div>
        </div>
        </>
    )
}
