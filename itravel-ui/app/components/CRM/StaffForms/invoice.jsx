"use client"
import { Axios } from "axios"
import { useState, useEffect } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPlus } from "@fortawesome/free-solid-svg-icons"
import Nav from "../Nav"
import { Heading } from "@kiwicom/orbit-components"
import "./styles.css"

export function Invoice() {
    const [fullName, setFullName]                       = useState("");
    const [mobileNumber, setMobileNumber]               = useState("");
    const [emailAddress, setEmailAddress]               = useState("");
    const [address, setAddress]                         = useState("");
    const [comments, setComments]                       = useState("");
    const [paymentInstructions, setPaymentInstructions] = useState("");
    
    let mydate      = new Date();
    let newdate     = mydate.getDate();
    let month       = mydate.getMonth() + 1;
    let year        = mydate.getFullYear();
    if(newdate < 10) {
        newdate     = "0" + newdate;
    }

    if (month < 10) {
        month       = "0" + month;
    }

    newdate         = newdate.toString().padStart(2, "0");
    month           = month.toString().padStart(2, "0");

    const [date, setDate]                               = useState(`${newdate}/${month}/${year}`);
    const [terms, setTerms]                             = useState("");
    const [dueDate, setDueDate]                         = useState("");
    const [subTotal, setSubTotal]                       = useState("");
    const [discount, setDiscount]                       = useState("");
    const [total, setTotal]                             = useState("");
    const [paid, setPaid]                               = useState("");
    const [method, setMethod]                           = useState("");
    const [balance, setBalance]                         = useState("");
    const [itemValues, setItemValues]                   = useState([{
                                                                        referenceNumber: "",
                                                                        itemDescription: "",
                                                                        quantity: "",
                                                                        price: "",
                                                                        totalPrice:""
                                                                    }])

    const addInvoice = ()=>{
        Axios.post("http://localhost:3002/api/create", {
            fullName: fullName,
            mobileNumber: mobileNumber, 
            emailAddress: emailAddress, 
            address: address, 
            comments: comments, 
            paymentInstructions: 
            paymentInstructions, 
            date: date, 
            terms: terms, 
            dueDate: dueDate, 
            subTotal: subTotal, 
            discount: discount, 
            total: total, 
            paid: paid, 
            method: method, 
            balance: balance, 
            referenceNumber: referenceNumber, 
            itemDescription: itemDescription, 
            quantity: quantity, 
            price: price, 
            total:total})
    }

    let handleChange = (i, e) => {
        let newItem = [...itemValues];
        newItem[i][e.target.name] = e.target.value;
        setItemValues(newItem);
    }

    let addItem = () => {
        setItemValues([...itemValues, {}])
    }

    let removeItem = (i) => {
        let newItem = [...itemValues];
        newItem.splice(i, 1);
        setItemValues(newItem);
    }

    return (
        <>
        <div className="flex flex-row mb-0" style={{"position": "relative"}}>
            <Nav />
            <div style={{"marginLeft": "200px"}} className="w-full">
                <div className="container mx-auto py-20">
                    <div className="mb-10">
                        <Heading as="h1" type="title2">Invoice</Heading>
                    </div>
            <form>
                <div className="flex flex-col mb-5 w-full gap-4">
                    <div className="text-center text-2xl mb-5" style={{["color"]: "#2489b5"}}>
                        <h2><b>Client Details</b></h2>
                    </div>

                    <div className="flex flex-row gap-8 w-full">
                        <div className="basis-1/2">
                            <label className='font-bold text-slate-500'>Full Name</label><br />
                            <input type="text" name="fullName" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setFullName(e.target.value)}}
                                placeholder="Enter Full Name" />
                        </div>

                        <div className="basis-1/2">
                            <label className='font-bold text-slate-500'>Mobile Number</label><br />
                            <input type="text" name="mobileNumber" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setMobileNumber(e.target.value)}}
                                placeholder="Enter Mobile Number" />
                        </div>
                    </div>

                    <div className="flex flex-row gap-8 w-full">
                        <div className="basis-1/2">
                            <label className='font-bold text-slate-500'>Email Address</label><br />
                            <input type="text" name="emailAddress" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setEmailAddress(e.target.value)}}
                                placeholder="Enter Email Address" />
                        </div>

                        <div className="basis-1/2">
                            <label className='font-bold text-slate-500'>Address</label><br />
                            <input type="text" name="address" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setAddress(e.target.value)}}
                                placeholder="Enter Address" />
                        </div>
                    </div>
                </div>

                <div className="flex flex-col mb-5 w-full gap-4">
                    <div className="text-center text-2xl mb-5 mt-5" style={{["color"]: "#2489b5"}}>
                        <h2><b>Item Details</b></h2>
                    </div>

                    {itemValues.map((element, index) => (
                    <div key={index} className="py-3 myAppendedItem">
                        <div>
                            <label className='font-bold text-slate-500'>Reference Number</label><br />
                            <input type="text" name="referenceNumber" 
                                className='pl-3 border py-1 rounded-sm w-full shadow'
                                value={element.referenceNumber || ""}
                                placeholder="Enter reference number" 
                                onChange={(e)=>{handleChange(index, e)}} />
                        </div>

                        <div>
                            <label className='font-bold text-slate-500'>Item Description</label><br />
                            <textarea name="itemDescription" rows="8" 
                                className='pl-3 border py-1 rounded-sm w-full shadow'
                                value={element.itemDescription || ""}
                                placeholder="Enter item description"
                                onChange={(e)=>{handleChange(index, e)}}></textarea>
                        </div>

                        <div className="flex flex-row gap-8 w-full">
                            <div className="basis-1/3">
                                <label className='font-bold text-slate-500'>Quantity</label><br />
                                <input type="text" name="quantity" 
                                    className='pl-3 border py-1 rounded-sm w-full shadow'
                                    value={element.quantity || ""}
                                    placeholder="£ 0.00"
                                    onChange={(e)=>{handleChange(index, e)}} />
                            </div>

                            <div className="basis-1/3">
                                <label className='font-bold text-slate-500'>Price</label><br />
                                <input type="text" name="price" 
                                    className='pl-3 border py-1 rounded-sm w-full shadow'
                                    value={element.price || ""}
                                    placeholder="£ 0.00"
                                    onChange={(e)=>{handleChange(index, e)}} />
                            </div>

                            <div className="basis-1/3">
                                <label className='font-bold text-slate-500'>Total</label><br />
                                <input type="text" name="totalPrice" readonly
                                    className='pl-3 border py-1 rounded-sm w-full'
                                    value={element.totalPrice || "0.00"}
                                    onChange={(e)=>{handleChange(index, e)}} />
                            </div>
                        </div>
                        {
                            index ?
                            <div className='flex flex-row-reverse'>
                                <button type="button" className="p-3 rounded-md mt-5" 
                                    style={{"color": "white", "backgroundColor": "#ff4d4d"}}
                                    onClick={() => removeItem(index)}>
                                    <i>Remove Item</i></button>
                            </div>
                            : null
                        }
                    </div>
                    ))}

                </div>

                <div className="flex flex-row w-full my-5">
                    <button id="appendItem" type="button" 
                        className="p-3 rounded-sm" 
                        style={{"color": "#d4ecf7", "backgroundColor": "#2489b5"}}
                        onClick={() => addItem()}>
                        <FontAwesomeIcon icon={faPlus} /> Add Item</button>
                </div>

                
                <div className="text-center text-2xl mb-5 mt-5" style={{["color"]: "#2489b5"}}>
                    <h2><b>Comment</b></h2>
                </div>

                <div>
                    <label className='font-bold text-slate-500'>Comments</label><br />
                    <textarea name="comments" rows="8" className='pl-3 border py-1 rounded-sm w-full shadow'
                        onChange={(e)=>{setComments(e.target.value)}}
                        placeholder="Enter comments here..."></textarea>
                </div>

                <div className="text-center text-2xl mb-5 mt-5" style={{["color"]: "#2489b5"}}>
                    <h2><b>Payment Instructions</b></h2>
                </div>

                <div>
                    <label className='font-bold text-slate-500'>Payment Instruction</label><br />
                    <textarea name="paymentInstructions" rows="8" className='pl-3 border py-1 rounded-sm w-full shadow'
                        onChange={(e)=>{setPaymentInstructions(e.target.value)}}
                        placeholder="Enter payment instructions..."></textarea>
                </div>

                <div className="flex flex-col gap-4 mb-5 w-full">
                    <div className="text-center text-2xl mb-5 mt-5" style={{["color"]: "#2489b5"}}>
                        <h2><b>Payment Details</b></h2>
                    </div>

                    <div className="flex flex-row gap-8 w-full">
                        <div className="basis-1/3">
                            <label className='font-bold text-slate-500'>Date</label><br />
                            <input type="text" name="date" className='pl-3 border py-1 rounded-sm w-full'
                                value={date} onChange={(e)=>{setDate(e.target.value)}} readonly />
                        </div>

                        <div className="basis-1/3">
                            <label className='font-bold text-slate-500'>Terms</label><br />
                            <input type="text" name="terms" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setTerms(e.target.value)}}
                                placeholder="Enter terms" />
                        </div>

                        <div className="basis-1/3">
                            <label className='font-bold text-slate-500'>Due Date</label><br />
                            <input type="text" name="dueDate" 
                                className='pl-3 border py-1 rounded-sm w-full shadow'
                                onClick={e => e.currentTarget.type = "date"}
                                onBlur={e => {!e.currentTarget.value ? e.currentTarget.type = "text" : e.currentTarget.type = "date"}}
                                onChange={(e)=>{setDueDate(e.target.value)}} />
                        </div>
                    </div>

                    <div className="flex flex-row gap-8 w-full">
                        <div className="basis-1/3">
                            <label className='font-bold text-slate-500'>Sub-Total</label><br />
                            <input type="text" name="subTotal" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setSubTotal(e.target.value)}} />
                        </div>

                        <div className="basis-1/3">
                            <label className='font-bold text-slate-500'>Discount</label><br />
                            <input type="text" name="discount" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setDiscount(e.target.value)}} />
                        </div>

                        <div className="basis-1/3">
                            <label className='font-bold text-slate-500'>Total</label><br />
                            <input type="text" name="total" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setTotal(e.target.value)}} />
                        </div>
                    </div>

                    <div className="flex flex-row gap-8 w-full">
                        <div className="basis-1/3">
                            <label className='font-bold text-slate-500'>Paid</label><br />
                            <input type="text" name="paid" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setPaid(e.target.value)}} />
                        </div>

                        <div className="basis-1/3">
                            <label className='font-bold text-slate-500'>Method</label><br />
                            <input type="text" name="method" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setMethod(e.target.value)}} />
                        </div>

                        <div className="basis-1/3">
                            <label className='font-bold text-slate-500'>Balance</label><br />
                            <input type="text" name="balance" className='pl-3 border py-1 rounded-sm w-full shadow'
                                onChange={(e)=>{setBalance(e.target.value)}} />
                        </div>
                    </div>
                </div>

                <div>
                    <input type="submit" name="submit" value="Save Invoice" 
                        style={{["backgroundColor"]: "#2489b5"}} className="py-2 font-bold rounded-md w-full text-center text-white"
                        onClick={addInvoice} />
                </div>
            </form>
                </div>
            </div>
        </div>
        </>
    )
}
