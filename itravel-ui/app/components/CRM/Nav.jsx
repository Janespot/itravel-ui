"use client"
import React from 'react';
import Link from 'next/link';

function Nav() {
    return (
        <div style={{"backgroundColor": "#2489b5", "color": "white", "position": "absolute", "width": "200px", "paddingTop": "100px"}} className='px-5 py-20 h-full min-h-screen flex flex-col gap-4 border-2 border-b'>
            <Link href="/CRM/itineraryForm">Itinerary Form</Link>
            <Link href="/CRM/enquiriesForm">Enquiry Form</Link>
            <Link href="/CRM/invoiceForm">Invoice Form</Link>
        </div>
    )
}

export default Nav
