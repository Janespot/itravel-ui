"use client"
import React from 'react';
import Link from 'next/link';

function Nav() {
    return (
        <div style={{["backgroundColor"]: "#2489b5", "color": "white"}} className='pl-5 py-20 h-screen flex flex-col w-48 mb-10 border-2 border-b'>
            <Link href="/enquiries" className='pb-5 text-primary'>Enquiries</Link>
            <Link href="/itinerary" className='pb-5'>Itinerary</Link>
            <Link href="/invoices" className="pb-5">Invoices</Link>
            <Link href="/clients" className='pb-5'>Clients</Link>
            <Link href="/staff" className='pr-b'>Staff</Link>
        </div>
    )
}

export default Nav
